import { BrowserRouter as Router, Route, Routes  } from 'react-router-dom'
// import Navebar from './componentes/Navebar'
import Sidebar from './componentes/Sidebar'
import Home from './paginas/Home'
import Housingdate from './paginas/Housingdate'
import Childdate from './paginas/Childdate'
import Supplies from './paginas/Supplies'
import Churches from './paginas/Churches'
import logoCaritas from './assets/img/logoCaritas.jpg'
import './App.css';
import State from './paginas/State'


function App() {

  return (
    <Router>
      <div className="flex">
        <div className="lateral">
          <div className="logo">
            <img src={logoCaritas} alt="logoCaritas" />
          </div>
          <Sidebar />
        </div>
        
        <div className="content">
          <div className="superior">
            {/* <Navebar /> */}
          </div>
          
            <Routes>
              <Route path="/" element={<Home/>} />
              <Route path="/state" element={<State/>} />
              <Route path="/housingdate" element={<Housingdate/>} />
              <Route path="/childdate" element={<Childdate/>} />
              <Route path="/supplies" element={<Supplies/>} />
              <Route path="/churches" element={<Churches/>} />
            </Routes>
        </div>
      </div> 
    </Router>
  );
}

export default App;
