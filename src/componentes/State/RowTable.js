import React from 'react'
import { RiCloseCircleLine } from 'react-icons/ri';
import { TiEdit } from 'react-icons/ti';
import { TableCell, TableRow, Typography } from '@mui/material';
// import FormState from './FormState';


import FormEdit from './FormEdit';
import ModalEdit from '../Modal/ModalEdit';
import { useModalEdit } from '../Modal/useModalEdit';
import FormState from './FormState';
import Modal from '../Modal/Modal';
// import FormState from './FormState';


const RowTable = ({el, data, setDataToEdit, deleteDataState, updateDataState, dataToEdit, createDataState}) => {
    let {nameState, id} = el;  

    const [isOpenModal1, openModal1, closeModal1] = useModalEdit(false);

    const edit = () => {
        openModal1()
        

    }

    // const modalEdit = () => {
        
    //     setDataToEdit(el)
    // }




    return(
        <div className='lista'>
            <TableCell component="th" scope="row">
                {nameState}
            </TableCell> 
            <TableCell align="center">                  
                <TiEdit
                    //  onClick={(setDataToEdit) => openModal2}

                    onClick={() => setDataToEdit(el)}
                    className='edit-icon'
                />  
                    <button onClick={edit} className='watch-icon'>
                        modal
                    </button>
                    <Modal isOpen={isOpenModal1} closeModal={closeModal1}>
                        <Typography>
                            <FormEdit
                                createDataState={createDataState}
                                updateDataState={updateDataState}
                                dataToEdit={dataToEdit}
                                setDataToEdit={setDataToEdit} 
                            />
                        </Typography>        
                    </Modal>
            



                <RiCloseCircleLine
                    onClick={() => deleteDataState(id)}
                    className='delete-icon'
                />
            </TableCell>                           
        </div>
    ) 
}

export default RowTable

