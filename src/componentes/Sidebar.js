import { NavLink } from 'react-router-dom'
import * as FaIcons from 'react-icons/fa'
import * as MdIcons from 'react-icons/md'
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem, } from 'reactstrap';


const Sidebar = () => {
 
    return (
        <div className="sidebar bg-gray">
            <ul> 
                <li>
                    <NavLink to="/" exact className='text-dark rounded py-2 w-100 d-inline-block px-3' activeClassName='active'><FaIcons.FaHome className='me-2'/> Inicio</NavLink>
                </li>

                <li>
                    <UncontrolledDropdown nav inNavbar>
                        <DropdownToggle exact className='text-dark rounded py-2 w-100 d-inline-block px-3' activeClassName='active' nav caret>
                            <MdIcons.MdEngineering className='me-2'/> Administrador
                        </DropdownToggle>

                        <DropdownMenu right>
                            <DropdownItem>
                                <NavLink to="/state" exact className='text-dark rounded py-2 w-100 d-inline-block px-3' activeClassName='active'> Estados </NavLink>
                                
                            </DropdownItem>
                            
                            <DropdownItem>
                                Municipios
                            </DropdownItem>

                            <DropdownItem>
                                Parroquias
                            </DropdownItem>
                            
                            <DropdownItem>
                                Comunidades
                            </DropdownItem>

                            <DropdownItem>
                                Sectores
                            </DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                </li>
            
                <li>
                    <UncontrolledDropdown nav inNavbar>
                        <DropdownToggle exact className='text-dark rounded py-2 w-100 d-inline-block px-3' activeClassName='active' nav caret>
                            <FaIcons.FaClipboardList className='me-2'/> Datos Vivienda
                        </DropdownToggle>

                        <DropdownMenu right>
                            <DropdownItem>
                                <NavLink to="/housingdate" exact className='text-dark rounded py-2 w-100 d-inline-block px-3' activeClassName='active'> Crear Vivienda </NavLink>
                                
                            </DropdownItem>
                            
                            <DropdownItem>
                                Listar Viviendas
                            </DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                </li>

                <li>
                    <UncontrolledDropdown nav inNavbar>
                        <DropdownToggle exact className='text-dark rounded py-2 w-100 d-inline-block px-3' activeClassName='active' nav caret>
                            <FaIcons.FaChild className='me-2'/> Datos Niños
                        </DropdownToggle>

                        <DropdownMenu right>
                            <DropdownItem>
                                <NavLink to="/childdate" exact className='text-dark rounded py-2 w-100 d-inline-block px-3' activeClassName='active'> Diagnostico Inicial</NavLink>
                            </DropdownItem>
                            
                            <DropdownItem>
                                Monitoreo
                            </DropdownItem>
                            
                            <DropdownItem>
                                Listar Niños
                            </DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                </li>
            
                <li>
                    <UncontrolledDropdown nav inNavbar>
                        <DropdownToggle exact className='text-dark rounded py-2 w-100 d-inline-block px-3' activeClassName='active' nav caret>
                            <FaIcons.FaUserShield className='me-2'/> Insumos
                        </DropdownToggle>

                        <DropdownMenu right>
                            <DropdownItem>
                            <NavLink to="/supplies" exact className='text-dark rounded py-2 w-100 d-inline-block px-3' activeClassName='active'> Listar Insumos</NavLink>
                            </DropdownItem>

                            <DropdownItem>
                                Repartir Insumos
                            </DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                </li>

                <li>
                    <UncontrolledDropdown nav inNavbar>
                        <DropdownToggle exact className='text-dark rounded py-2 w-100 d-inline-block px-3' activeClassName='active' nav caret>
                        <FaIcons.FaChurch className='me-2'/> Iglesias
                        </DropdownToggle>

                        <DropdownMenu right>
                            <DropdownItem>
                            <NavLink to="/churches" exact className='text-dark rounded py-2 w-100 d-inline-block px-3' activeClassName='active'> Parrocos</NavLink>
                            </DropdownItem>

                            <DropdownItem>
                                Grupos Parroquiales
                            </DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                </li>                
            </ul>
        </div>
    )
}

export default Sidebar