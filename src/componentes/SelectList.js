import React from 'react'
import TextField from '@mui/material/TextField';
import MenuItem from '@mui/material/MenuItem';
import Paper from '@mui/material/Paper';


const options = [
    {
      value: 'USD',
      label: '$',
    },
    {
      value: 'EUR',
      label: '€',
    },
    {
      value: 'BTC',
      label: '฿',
    },
    {
      value: 'JPY',
      label: '¥',
    },
  ];

const SelectList = () => {
    const [option, setOptions] = React.useState('EUR');

    const handleChange = (event) => {
      setOptions(event.target.value);
    };
    return (
        <div className='SelectList'> 
              <Paper
      component="form"
      sx={{ p: '200px 130px', display: 'flex', alignItems: 'center', width: 900 }}
    >
        <TextField
            id="outlined-select-currency"
            select
            style={{minWidth: 600}}
            // fullWidth
            label="Estado"
            value={option}
            onChange={handleChange}
            helperText="Seleccione el estado"
            >
                {options.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                        {option.label}
                    </MenuItem>
                ))}
            </TextField>
            </Paper>
        </div>
    )
}

export default SelectList
