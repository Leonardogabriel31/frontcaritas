import React, { useState } from 'react';
import * as FaIcons from 'react-icons/fa'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem, } from 'reactstrap';

const Navebar = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
    return (
      <div>
        <Navbar color="light" light expand="md">
          <NavbarBrand href="/">reactstrap</NavbarBrand>
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="ms-auto" navbar>
              
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                <FaIcons.FaUserCircle className='me-2'/> Administrador
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    Mi Perfil
                  </DropdownItem>
                  <DropdownItem>
                    Configuraciones
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>
                    Cerrar sesion
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
            
          </Collapse>
        </Navbar>
      </div>
    );
}

export default Navebar;