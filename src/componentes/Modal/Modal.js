import { Button } from "@mui/material";
import "./Modal.css";


const Modal = ({children, isOpen, closeModal}) => {
  const handleModalContainerClick = (event) => event.stopPropagation();
  return(
    
      <article className={`modal ${isOpen && "is-open"}`} onClick={closeModal}>
        <div className="modal-container" onClick={handleModalContainerClick}>
        
          {children}
        
        
        <div className="footer">
          <Button 
            variant="contained" 
            color="primary"            
            onClick={closeModal}
          >
            Continuar
          </Button>
        </div>
        </div>
      </article>

  )
}

export default Modal;