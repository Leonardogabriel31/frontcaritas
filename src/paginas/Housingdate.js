import { MenuItem, Select } from "@mui/material"
import React from "react"


const HousingDate = () => {
    
    const [course, setCourse] = React.useState('')

    const updateSelectVal = (e) => {
        setCourse(e.target.value)
    }

    const updateSelect2Val = (e) => {
        setCourse(e.target.value)
    }
    const updateSelect3Val = (e) => {
        setCourse(e.target.value)
    }
    
    return(
        <div className="housingdateUbicate">
            <h1>Crear Vivienda</h1>
            <h4>Ubicacion</h4>
            <div className="selectUbicate">            
                <div className="Parroquia">
                    <label>Parroquia</label>
                    <div className="selectParroquia">
                        <Select value={course} 
                            displayEmpty
                            onChange={updateSelectVal}
                        >
                            <MenuItem value="" disabled>Seleccione una parroquia</MenuItem>
                            <MenuItem value={1}>Node</MenuItem>
                            <MenuItem value={2}>PHP</MenuItem>
                            <MenuItem value={3}>Java</MenuItem>
                            <MenuItem value={4}>Javascript</MenuItem>
                        
                        </Select>
                    </div>
                </div>

                <div className="Comunidad">
                    <label>Comunidad</label>
                    <div className="selectComunidad">
                        <Select value={course} 
                            displayEmpty
                            onChange={updateSelect2Val}
                        >
                            <MenuItem value="" disabled>Seleccione una comunidad</MenuItem>
                            <MenuItem value={1}>Node</MenuItem>
                            <MenuItem value={2}>PHP</MenuItem>
                            <MenuItem value={3}>Java</MenuItem>
                            <MenuItem value={4}>Javascript</MenuItem>
                        
                        </Select>
                    </div>
                </div>

                <div className="Sector">
                    <label>Sector</label>
                    <div className="selectSector">
                        <Select value={course} 
                            displayEmpty
                            onChange={updateSelect3Val}
                        >
                            <MenuItem value="" disabled>Seleccione un sector</MenuItem>
                            <MenuItem value={1}>Node</MenuItem>
                            <MenuItem value={2}>PHP</MenuItem>
                            <MenuItem value={3}>Java</MenuItem>
                            <MenuItem value={4}>Javascript</MenuItem>
                        
                        </Select>
                    </div>
                </div>
            </div>
        </div>        
    )
}

export default HousingDate