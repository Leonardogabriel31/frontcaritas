import React, { useState } from 'react'
import TableState from '../componentes/State/TableState';
import Modal from '../componentes/Modal/Modal';
import { Typography } from '@mui/material';
import { useModal } from '../componentes/Modal/useModal';
import FormState from '../componentes/State/FormState';


const initialDb = [
    {
        id: 1,
        nameState: "Sucre",
    },
    {
        id: 2,
        nameState: "Anzoategui",
    },
    {
        id: 3,
        nameState: "Monagas",
    },
    {
        id: 4,
        nameState: "Delta Amacuro",
    },
    {
        id: 5,
        nameState: "Nva Esparta",
    }
];

const State = () => {
  const [db, setDb] = useState(initialDb);

  const [dataToEdit, setDataToEdit] = useState(null);

  const [isOpenModal1, openModal1, closeModal1] = useModal(false);

  const createDataState = (data) => {
    data.id = Date.now();
    setDb([...db, data])
    // console.log(db);
  };

  const updateDataState = (data) => {
    let newData = db.map((el) => el.id === data.id ? data : el);
    setDb(newData);
  };

  const deleteDataState = (id) => {
    let newData = db.filter((el) => el.id !== id);
    setDb(newData);
  };

    

  return (
    <div>
      estados

      <TableState 
        data={db}
        setDataToEdit={setDataToEdit}
        deleteDataState={deleteDataState}
      />
      {/* MODAL */}
      <Modal isOpen={isOpenModal1} closeModal={closeModal1}>
        <Typography>
          <FormState 
            createDataState={createDataState}
            updateDataState={updateDataState}
            dataToEdit={dataToEdit}
            setDataToEdit={setDataToEdit} 
          />
        </Typography>        
      </Modal>
      <button onClick={openModal1} className='watch-icon'>
        modal
      </button>

      {/* <SelectsAnidados /> */}
      
    </div>
  )
}

export default State
